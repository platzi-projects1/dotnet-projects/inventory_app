using Entities;
using DataAccess;

using Microsoft.EntityFrameworkCore;

namespace Business
{
    public class B_Category
    {

        private readonly IDbContextFactory<inventory_context> _ContextFactory;
        public B_Category(IDbContextFactory<inventory_context> ctx)
        {
            _ContextFactory = ctx;
        }
        public async Task<List<Category>> GetCategories()
        {
            using var db = _ContextFactory.CreateDbContext();            
            return await db.Categories.OrderBy(c => c.CatId).ToListAsync();;
        }

        public async Task CreateCategory(Category oCategory)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Categories.Add(oCategory);
            await db.SaveChangesAsync();
        }

        public async Task<Category> GetCategoryById(int catId)
        {
            using var db = _ContextFactory.CreateDbContext();
            return await db.Categories.OrderBy(c => c.CatId).FirstAsync(c => c.CatId == catId);
        }

        public async Task UpdateCategory(Category oCategory)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Categories.Update(oCategory);
            await db.SaveChangesAsync();
        }
    }
}