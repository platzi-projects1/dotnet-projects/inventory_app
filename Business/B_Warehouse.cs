using Entities;
using DataAccess;

using Microsoft.EntityFrameworkCore;

namespace Business
{
    public class B_Warehouse
    {
        private readonly IDbContextFactory<inventory_context> _ContextFactory;

        public B_Warehouse(IDbContextFactory<inventory_context> ctx)
        {
            _ContextFactory = ctx;
        }
        public async Task<List<Warehouse>> GetWarehouses()
        {
            using var db = _ContextFactory.CreateDbContext();
            return await db.Warehouses.OrderBy(w => w.WarehouseId).ToListAsync();
        }

        public async Task CreateWarehouse(Warehouse oWarehouse)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Warehouses.Add(oWarehouse);
            await db.SaveChangesAsync();
        }

        public async Task<Warehouse> GetWarehouseById(int wid)
        {
            using var db = _ContextFactory.CreateDbContext();
            return await db.Warehouses.OrderBy(w => w.WarehouseId).FirstAsync(w => w.WarehouseId == wid);
        }

        public async Task UpdateWarehouse(Warehouse oWarehouse)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Warehouses.Update(oWarehouse);
            await db.SaveChangesAsync();
        }
    }
}
