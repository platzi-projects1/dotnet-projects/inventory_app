
using System;
using System.Collections.Generic;

namespace Entities
{
    public partial class Storage
    {

        public Storage()
        {
            // product_list = new HashSet<Product>(); 
            // warehouses_list = new HashSet<Warehouse>();
            transactions = new HashSet<Storage_Transaction>();
        }
        public int StorageId { get; set; }
        public int? ProdId { get; set; }
        public int? WarehouseId { get; set; }
        public DateTime? LastUpdate { get; set; }
        public int? PartialQuantity { get; set; }

        // public virtual ICollection<Product> product_list { get; set; }
        public virtual Product? Product { get; set; }
        // public virtual ICollection<Warehouse> warehouses_list { get; set; }
        public virtual Warehouse? Warehouse { get; set; }
        public virtual ICollection<Storage_Transaction> transactions { get; set; }
        
    }
}