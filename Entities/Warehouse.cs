using System;
using System.Collections.Generic;

namespace Entities
{
    public partial class Warehouse
    {
        public Warehouse()
        {
            storages = new HashSet<Storage>();
            products = new HashSet<Product>(); 
        }
        
        public int WarehouseId { get; set; }        
        
        public string WarehouseName { get; set; } = null!;
        public string WarehouseAddress { get; set; } = null!;

        public virtual ICollection<Product> products { get; set; }
        public virtual ICollection<Storage> storages { get; set; }
        
    }
}