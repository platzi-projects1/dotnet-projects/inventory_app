﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class AddProductInfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "transaction_date",
                table: "storage_transactions",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 6, 23, 50, 28, 441, DateTimeKind.Utc).AddTicks(2929),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 4, 17, 9, 6, 696, DateTimeKind.Utc).AddTicks(1602));

            migrationBuilder.AddColumn<bool>(
                name: "is_product_in",
                table: "storage_transactions",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "is_product_in",
                table: "storage_transactions");

            migrationBuilder.AlterColumn<DateTime>(
                name: "transaction_date",
                table: "storage_transactions",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 4, 17, 9, 6, 696, DateTimeKind.Utc).AddTicks(1602),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 6, 23, 50, 28, 441, DateTimeKind.Utc).AddTicks(2929));
        }
    }
}
