using Entities;
using DataAccess;

using Microsoft.EntityFrameworkCore;

namespace Business
{
    public class B_Storage
    {
        private readonly IDbContextFactory<inventory_context> _ContextFactory;

        public B_Storage(IDbContextFactory<inventory_context> ctx)
        {
            _ContextFactory = ctx;
        }
        public async Task<List<Storage>> GetStorages()
        {
            using var db = _ContextFactory.CreateDbContext();
            return await db.Storages.OrderBy(s => s.StorageId).ToListAsync();
        }

        public async Task CreateStorage(Storage oStorage)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Storages.Add(oStorage);
            await db.SaveChangesAsync();
        }

        public async Task<bool> IsProductInWarehouse(int prod_id, int warehouse_id)
        {
            using var db = _ContextFactory.CreateDbContext();
            
            var product_already_in_warehouse = false;            

            var storages = await this.GetStorages();

            foreach (var storage in storages)
            {
                if (storage is not null)
                {
                    if ((storage.ProdId == prod_id) && (storage.WarehouseId == warehouse_id))
                    {
                        product_already_in_warehouse = true;
                    }
                }
            }

            return product_already_in_warehouse;
            
        }

        public async Task<List<Storage>> GetProductsByWarehouse(int warehouse_id)
        {
            using var db = _ContextFactory.CreateDbContext();
            
            return await db.Storages
                        .Include(s => s.Product)
                        .Include(s => s.Warehouse)
                        .Where(s => s.WarehouseId == warehouse_id)
                        .ToListAsync();                        
        }

        public async Task<List<Storage>> GetWarehousesByProduct(int product_id)
        {
            using var db = _ContextFactory.CreateDbContext();

            return await db.Storages
                        .Include(s => s.Product)
                        .Include(s => s.Warehouse)
                        .Where(s => s.ProdId == product_id)
                        .ToListAsync();                      
        }

        public async Task UpdateStorage(Storage oStorage)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Storages.Update(oStorage);
            await db.SaveChangesAsync();
        }
    }
}