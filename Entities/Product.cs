﻿using System;
using System.Collections.Generic;

namespace Entities
{
    public partial class Product
    {
        public Product()
        {
            storages = new HashSet<Storage>();
            warehouses = new HashSet<Warehouse>();
        }
        public int ProdId { get; set; }
        public int? CatId { get; set; }
        public string ProdCode { get; set; } = null!;
        public string ProdName { get; set; } = null!;
        public string ProdDescription { get; set; } = null!;
        public int? TotalQuantity { get; set; }

        public virtual Category? Category { get; set; }
        // public virtual Warehouse? Warehouse { get; set; }
        public virtual ICollection<Warehouse> warehouses { get; set; }
        public virtual ICollection<Storage> storages { get; set; }
    }
}
