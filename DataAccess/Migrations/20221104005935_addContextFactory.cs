﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class addContextFactory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "categories",
                columns: table => new
                {
                    cat_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    cat_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    cat_name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("categories_pkey", x => x.cat_id);
                });

            migrationBuilder.CreateTable(
                name: "warehouses",
                columns: table => new
                {
                    WarehouseId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    warehouse_name = table.Column<string>(type: "character varying(120)", maxLength: 120, nullable: false),
                    warehouse_address = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("warehouses_pkey", x => x.WarehouseId);
                });

            migrationBuilder.CreateTable(
                name: "products",
                columns: table => new
                {
                    prod_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    cat_id = table.Column<int>(type: "integer", nullable: true),
                    prod_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    prod_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    prod_description = table.Column<string>(type: "character varying(600)", maxLength: 600, nullable: false),
                    total_quantity = table.Column<int>(type: "integer", nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("products_pkey", x => x.prod_id);
                    table.ForeignKey(
                        name: "fk_category",
                        column: x => x.cat_id,
                        principalTable: "categories",
                        principalColumn: "cat_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "storages",
                columns: table => new
                {
                    StorageId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    prod_id = table.Column<int>(type: "integer", nullable: false),
                    warehouse_id = table.Column<int>(type: "integer", nullable: false),
                    last_update = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    partial_quantity = table.Column<int>(type: "integer", nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("storages_pkey", x => x.StorageId);
                    table.ForeignKey(
                        name: "FK_storages_products_prod_id",
                        column: x => x.prod_id,
                        principalTable: "products",
                        principalColumn: "prod_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_storages_warehouses_warehouse_id",
                        column: x => x.warehouse_id,
                        principalTable: "warehouses",
                        principalColumn: "WarehouseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "storage_transactions",
                columns: table => new
                {
                    STransactionId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    storage_id = table.Column<int>(type: "integer", nullable: true),
                    transaction_date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false, defaultValue: new DateTime(2022, 11, 4, 0, 59, 35, 602, DateTimeKind.Utc).AddTicks(1923)),
                    prod_quantity = table.Column<int>(type: "integer", nullable: true, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("stransactions_pkey", x => x.STransactionId);
                    table.ForeignKey(
                        name: "FK_storage_transactions_storages_storage_id",
                        column: x => x.storage_id,
                        principalTable: "storages",
                        principalColumn: "StorageId");
                });

            migrationBuilder.InsertData(
                table: "categories",
                columns: new[] { "cat_id", "cat_code", "cat_name" },
                values: new object[,]
                {
                    { 1, "HG", "Hogar" },
                    { 2, "VJ", "Video Juegos" },
                    { 3, "ASH", "Aseo Hogar" },
                    { 4, "PFM", "Perfumeria" },
                    { 5, "SLD", "Salud" },
                    { 6, "ASP", "Aseo Personal" }
                });

            migrationBuilder.InsertData(
                table: "warehouses",
                columns: new[] { "WarehouseId", "warehouse_address", "warehouse_name" },
                values: new object[,]
                {
                    { 1, "Calle 6 # 8-76", "Bodega Norte" },
                    { 2, "Calle 90 # 22-189", "El botellon" },
                    { 3, "Avenida 87 # 22-88", "Micromercado Mercantil" }
                });

            migrationBuilder.InsertData(
                table: "products",
                columns: new[] { "prod_id", "cat_id", "prod_code", "prod_description", "prod_name" },
                values: new object[,]
                {
                    { 1, 1, "HG-MN", "Mesa de madera fina", "Mesita de Noche" },
                    { 2, 1, "HG-TP", "Tapete de algodon para comodidad de sus pies", "Tapete de Cama" },
                    { 3, 1, "HG-ER", "Espejo de repisa para colgar en la pared", "Espejo de repisa" },
                    { 4, 2, "VJ-CLD", "Juego de Shooter basado en la WWI", "Call of Duty" },
                    { 5, 2, "VJ-HL", "Juego de Shooter futurista", "Hallo" },
                    { 6, 2, "VJ-FF", "Juego de Deporte basado en la Liga de futbol de Colombia", "Fifa 2018" },
                    { 7, 3, "ASH-JR", "Jabon en barra para Ropa", "Jabon Rey" },
                    { 8, 3, "ASH-LP", "Detergente Desinfectante", "Limpido Clorox" },
                    { 9, 3, "ASH-CP", "Cepillo de cerda dura", "Cepillo de Limpieza" },
                    { 10, 4, "PFM-CHR", "Perfume de aroma suave maderal", "Carolina Herrera - 212 Masculino" },
                    { 11, 4, "PFM-JQ", "Perfume de aroma fuerte", "Jaque for Men" },
                    { 12, 4, "PFM-PRPH", "Perfume fino de a familia olfativa Oriental Amaderada", "Dolce & Gabbana Light Blue pour Homme" },
                    { 13, 5, "SLD-DL", "Medicamneto para controlar los sintomas de la gripa", "Dolex con Acetaminofen" },
                    { 14, 5, "PFM-AML", "Capsula para controlar la presión arterial", "Amlodipina 5 mg" },
                    { 15, 5, "PFM-TCR", "Cápsula de liberación controlada", "Tacrolimus de 2 mg" },
                    { 16, 6, "ASP-CCG", "Crema de limpieza dental", "Crema de dientes Colgate" },
                    { 17, 6, "PFM-LST", "Líquido para limpieza bucal", "Listerine" },
                    { 18, 6, "PFM-SHP", "Shampoo para limpieza de cabello", "Shampoo Head and Shoulders" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_products_cat_id",
                table: "products",
                column: "cat_id");

            migrationBuilder.CreateIndex(
                name: "IX_storage_transactions_storage_id",
                table: "storage_transactions",
                column: "storage_id");

            migrationBuilder.CreateIndex(
                name: "IX_storages_prod_id",
                table: "storages",
                column: "prod_id");

            migrationBuilder.CreateIndex(
                name: "IX_storages_warehouse_id",
                table: "storages",
                column: "warehouse_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "storage_transactions");

            migrationBuilder.DropTable(
                name: "storages");

            migrationBuilder.DropTable(
                name: "products");

            migrationBuilder.DropTable(
                name: "warehouses");

            migrationBuilder.DropTable(
                name: "categories");
        }
    }
}
