using System;
using System.Collections.Generic;

namespace Entities
{
    public partial class Storage_Transaction
    {

        
        public int STransactionId { get; set; }
        public int? StorageId { get; set; }
        
        public DateTime? TransactionDate { get; set; }
        public int? ProductQuantity { get; set; }
        public bool IsProductIn { get; set;}

        public virtual Storage? Storage { get; set; }
        
    }
}