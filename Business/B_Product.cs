
using Entities;
using DataAccess;

using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Business
{
    public class B_Product
    {
        
        private readonly IDbContextFactory<inventory_context> _ContextFactory;
        
        public B_Product (IDbContextFactory<inventory_context> ctx)
        {
            _ContextFactory = ctx;
        }

        public async Task<List<Product>> GetProducts()
        {

            using var db = _ContextFactory.CreateDbContext();
            return await db.Products.OrderBy(p => p.ProdId).ToListAsync();
        }

        public async Task CreateProduct(Product oProduct)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Products.Add(oProduct);
            await db.SaveChangesAsync();
        }

        public async Task<Product> GetProductById(int prod_id)
        {
            using var db = _ContextFactory.CreateDbContext();
            return await db.Products.OrderBy(p => p.ProdId)
                            .FirstAsync(p => p.ProdId == prod_id);
            // return await db.Products.FindAsync(p => p.ProdId == prod_id);
        }

        public async Task UpdateProduct(Product oProduct)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Products.Update(oProduct);
            await db.SaveChangesAsync();
        }
    }
}