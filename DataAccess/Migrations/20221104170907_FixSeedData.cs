﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class FixSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "transaction_date",
                table: "storage_transactions",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 4, 17, 9, 6, 696, DateTimeKind.Utc).AddTicks(1602),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 4, 0, 59, 35, 602, DateTimeKind.Utc).AddTicks(1923));

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 14,
                columns: new[] { "prod_code", "prod_name" },
                values: new object[] { "SLD-AML", "Amlodipino 5 mg" });

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 15,
                column: "prod_code",
                value: "SLD-TCR");

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 17,
                column: "prod_code",
                value: "ASP-LST");

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 18,
                column: "prod_code",
                value: "ASP-SHP");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "transaction_date",
                table: "storage_transactions",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 4, 0, 59, 35, 602, DateTimeKind.Utc).AddTicks(1923),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 4, 17, 9, 6, 696, DateTimeKind.Utc).AddTicks(1602));

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 14,
                columns: new[] { "prod_code", "prod_name" },
                values: new object[] { "PFM-AML", "Amlodipina 5 mg" });

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 15,
                column: "prod_code",
                value: "PFM-TCR");

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 17,
                column: "prod_code",
                value: "PFM-LST");

            migrationBuilder.UpdateData(
                table: "products",
                keyColumn: "prod_id",
                keyValue: 18,
                column: "prod_code",
                value: "PFM-SHP");
        }
    }
}
