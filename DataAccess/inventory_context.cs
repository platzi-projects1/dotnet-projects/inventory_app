﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

using Entities;

namespace DataAccess
{
    public partial class inventory_context : DbContext
    {
        public inventory_context()
        {
        }

        public inventory_context(DbContextOptions<inventory_context> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Product> Products { get; set; } = null!;

        public virtual DbSet<Storage> Storages { get; set; } = null!;
        public virtual DbSet<Warehouse> Warehouses { get; set; } = null!;
        public virtual DbSet<Storage_Transaction> Storage_Transactions { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(e => e.CatId)
                    .HasName("categories_pkey");

                entity.ToTable("categories");

                entity.Property(e => e.CatId)
                    .HasColumnName("cat_id");

                entity.Property(e => e.CatCode)
                    .HasMaxLength(50)
                    .HasColumnName("cat_code");

                entity.Property(e => e.CatName)
                    .HasMaxLength(50)
                    .HasColumnName("cat_name");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.ProdId)
                    .HasName("products_pkey");

                entity.ToTable("products");

                entity.Property(e => e.ProdId)
                    .HasColumnName("prod_id");

                entity.Property(e => e.CatId)
                    .HasColumnName("cat_id");

                
                entity.Property(e => e.ProdCode)
                    .HasMaxLength(50)
                    .HasColumnName("prod_code");

                entity.Property(e => e.ProdDescription)
                    .HasMaxLength(600)
                    .HasColumnName("prod_description");

                entity.Property(e => e.ProdName)
                    .HasMaxLength(100)
                    .HasColumnName("prod_name");

                entity.Property(e => e.TotalQuantity)
                    .HasColumnName("total_quantity")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CatId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("fk_category");

                entity.HasMany(p => p.warehouses)
                    .WithMany(p => p.products)
                    .UsingEntity<Storage>(
                        j => j
                            .HasOne(s => s.Warehouse)
                            .WithMany(w => w.storages)
                            .HasForeignKey(s => s.WarehouseId),
                        j => j
                            .HasOne(s => s.Product)
                            .WithMany(p => p.storages)
                            .HasForeignKey(s => s.ProdId),
                        j => 
                        {
                            j.HasKey(s => new {s.ProdId, s.WarehouseId  });
                        }
                    );
            });

            modelBuilder.Entity<Storage>( entity => 
                {
                    entity.HasKey(e => e.StorageId)
                        .HasName("storages_pkey");
                    
                    entity.ToTable("storages");

                    entity.Property(e => e.ProdId)
                        .HasColumnName("prod_id");

                    entity.Property(e => e.WarehouseId)
                        .HasColumnName("warehouse_id");

                    entity.Property(e => e.LastUpdate)
                        .HasColumnName("last_update")
                        .IsRequired();

                    entity.Property(e => e.PartialQuantity)
                        .HasColumnName("partial_quantity")
                        .HasDefaultValueSql("0");
                }
            );

            modelBuilder.Entity<Warehouse>( entity => 
                {
                    entity.HasKey(e => e.WarehouseId)
                        .HasName("warehouses_pkey");
                    
                    entity.ToTable("warehouses");

                    entity.Property(e => e.WarehouseName)
                        .HasColumnName("warehouse_name")
                        .HasMaxLength(120)
                        .IsRequired();
                    
                    entity.Property(e => e.WarehouseAddress)
                        .HasColumnName("warehouse_address")
                        .HasMaxLength(200)
                        .IsRequired();                  
                }
            );

            modelBuilder.Entity<Storage_Transaction>( entity => 
                {
                    entity.HasKey(e => e.STransactionId)
                        .HasName("stransactions_pkey");
                    
                    entity.ToTable("storage_transactions");

                    entity.Property(e => e.StorageId)
                        .HasColumnName("storage_id");

                    entity.Property(e => e.ProductQuantity)
                        .HasColumnName("prod_quantity")
                        .HasDefaultValueSql("0");
                    
                    entity.Property(e => e.IsProductIn)
                        .HasColumnName("is_product_in");
                        

                    entity.Property(e => e.TransactionDate)
                        .HasColumnName("transaction_date")
                        .HasDefaultValue(DateTime.UtcNow)
                        .IsRequired();
                }
            );


            modelBuilder.Seed();

            OnModelCreatingPartial(modelBuilder);
        }

        

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
