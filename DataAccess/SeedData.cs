using System;
using Microsoft.EntityFrameworkCore;

using Entities;

namespace DataAccess
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasData(
                    new Category { 
                        CatId = 1, CatCode = "HG", CatName = "Hogar" 
                    },
                    new Category { 
                        CatId = 2, CatCode = "VJ", CatName = "Video Juegos" 
                    },
                    new Category { 
                        CatId = 3, CatCode = "ASH", CatName = "Aseo Hogar" 
                    },
                    new Category { 
                        CatId = 4, CatCode = "PFM", CatName = "Perfumeria" 
                    },
                    new Category { 
                        CatId = 5, CatCode = "SLD", CatName = "Salud" 
                    },
                    new Category { 
                        CatId = 6, CatCode = "ASP", CatName = "Aseo Personal" 
                    }
                );
            modelBuilder.Entity<Product>()
                .HasData(
                    // Products Category Hogar
                    new Product {
                        ProdId = 1, CatId = 1, ProdCode = "HG-MN", ProdName = "Mesita de Noche", ProdDescription = "Mesa de madera fina"
                    },
                    new Product {
                        ProdId = 2, CatId = 1, ProdCode = "HG-TP", ProdName = "Tapete de Cama", ProdDescription = "Tapete de algodon para comodidad de sus pies"
                    },
                    new Product {
                        ProdId = 3, CatId = 1, ProdCode = "HG-ER", ProdName = "Espejo de repisa", ProdDescription = "Espejo de repisa para colgar en la pared"
                    },
                    // Products Category VideoJuegos
                    new Product {
                        ProdId = 4, CatId = 2, ProdCode = "VJ-CLD", ProdName = "Call of Duty", ProdDescription = "Juego de Shooter basado en la WWI"
                    },
                    new Product {
                        ProdId = 5, CatId = 2, ProdCode = "VJ-HL", ProdName = "Hallo", ProdDescription = "Juego de Shooter futurista"
                    },
                    new Product {
                        ProdId = 6, CatId = 2, ProdCode = "VJ-FF", ProdName = "Fifa 2018", ProdDescription = "Juego de Deporte basado en la Liga de futbol de Colombia"
                    },
                    // Products Category Aseo Hogar
                    new Product {
                        ProdId = 7, CatId = 3, ProdCode = "ASH-JR", ProdName = "Jabon Rey", ProdDescription = "Jabon en barra para Ropa"
                    },
                    new Product {
                        ProdId = 8, CatId = 3, ProdCode = "ASH-LP", ProdName = "Limpido Clorox", ProdDescription = "Detergente Desinfectante"
                    },
                    new Product {
                        ProdId = 9, CatId = 3, ProdCode = "ASH-CP", ProdName = "Cepillo de Limpieza", ProdDescription = "Cepillo de cerda dura"
                    },
                    // Products Category Perfumeria
                    new Product {
                        ProdId = 10, CatId = 4, ProdCode = "PFM-CHR", ProdName = "Carolina Herrera - 212 Masculino", ProdDescription = "Perfume de aroma suave maderal"
                    },
                    new Product {
                        ProdId = 11, CatId = 4, ProdCode = "PFM-JQ", ProdName = "Jaque for Men", ProdDescription = "Perfume de aroma fuerte"
                    },
                    new Product {
                        ProdId = 12, CatId = 4, ProdCode = "PFM-PRPH", ProdName = "Dolce & Gabbana Light Blue pour Homme", ProdDescription = "Perfume fino de a familia olfativa Oriental Amaderada"
                    },
                    // Products Category Salud
                    new Product {
                        ProdId = 13, CatId = 5, ProdCode = "SLD-DL", ProdName = "Dolex con Acetaminofen", ProdDescription = "Medicamneto para controlar los sintomas de la gripa"
                    },
                    new Product {
                        ProdId = 14, CatId = 5, ProdCode = "SLD-AML", ProdName = "Amlodipino 5 mg", ProdDescription = "Capsula para controlar la presión arterial"
                    },
                    new Product {
                        ProdId = 15, CatId = 5, ProdCode = "SLD-TCR", ProdName = "Tacrolimus de 2 mg", ProdDescription = "Cápsula de liberación controlada"
                    },
                    // Products Category Aseo Personal
                    new Product {
                        ProdId = 16, CatId = 6, ProdCode = "ASP-CCG", ProdName = "Crema de dientes Colgate", ProdDescription = "Crema de limpieza dental"
                    },
                    new Product {
                        ProdId = 17, CatId = 6, ProdCode = "ASP-LST", ProdName = "Listerine", ProdDescription = "Líquido para limpieza bucal"
                    },
                    new Product {
                        ProdId = 18, CatId = 6, ProdCode = "ASP-SHP", ProdName = "Shampoo Head and Shoulders", ProdDescription = "Shampoo para limpieza de cabello"
                    }
                );
            modelBuilder.Entity<Warehouse>()
                .HasData(
                    new Warehouse {
                        WarehouseId = 1, WarehouseName = "Bodega Norte", WarehouseAddress = "Calle 6 # 8-76"
                    },
                    new Warehouse {
                        WarehouseId = 2, WarehouseName = "El botellon", WarehouseAddress = "Calle 90 # 22-189"
                    },
                    new Warehouse {
                        WarehouseId = 3, WarehouseName = "Micromercado Mercantil", WarehouseAddress = "Avenida 87 # 22-88"
                    }
                );            

        }
    }
}