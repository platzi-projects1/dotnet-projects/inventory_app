using Entities;
using DataAccess;

using Microsoft.EntityFrameworkCore;

namespace Business
{
    public class B_STransactions
    {
        private readonly IDbContextFactory<inventory_context> _ContextFactory;

        public B_STransactions(IDbContextFactory<inventory_context> ctx)
        {
            _ContextFactory = ctx;
        }
        public async Task<List<Storage_Transaction>> GetStoragesTransactions()
        {
            using var db = _ContextFactory.CreateDbContext();
            return await db.Storage_Transactions.ToListAsync();
        }

        public async Task CreateTransaction(Storage_Transaction? oTransaction)
        {
            using var db = _ContextFactory.CreateDbContext();
            if (db is not null && oTransaction is not null)
            {
                db.Storage_Transactions.Add(oTransaction);
                await db.SaveChangesAsync();
            }
        }

        public async Task UpdateTransaction(Storage_Transaction oTransaction)
        {
            using var db = _ContextFactory.CreateDbContext();
            db.Storage_Transactions.Update(oTransaction);
            await db.SaveChangesAsync();
        }
    }
}