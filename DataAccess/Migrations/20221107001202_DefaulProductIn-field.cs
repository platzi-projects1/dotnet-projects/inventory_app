﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    public partial class DefaulProductInfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "transaction_date",
                table: "storage_transactions",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 7, 0, 12, 1, 706, DateTimeKind.Utc).AddTicks(8803),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 6, 23, 50, 28, 441, DateTimeKind.Utc).AddTicks(2929));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "transaction_date",
                table: "storage_transactions",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(2022, 11, 6, 23, 50, 28, 441, DateTimeKind.Utc).AddTicks(2929),
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldDefaultValue: new DateTime(2022, 11, 7, 0, 12, 1, 706, DateTimeKind.Utc).AddTicks(8803));
        }
    }
}
